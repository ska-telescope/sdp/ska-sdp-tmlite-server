Server functions
================

.. autofunction:: ska.tmlite.server.main.add_model

.. autofunction:: ska.tmlite.server.main.get_instrument

.. autofunction:: ska.tmlite.server.main.get_model

.. autofunction:: ska.tmlite.server.main.get_layout

.. autofunction:: ska.tmlite.server.main.get_static_rfi_mask

.. autofunction:: ska.tmlite.server.main.read_root

.. autofunction:: ska.tmlite.server.main.update_model_antennas

.. autofunction:: ska.tmlite.server.main.update_model_layout_from_file

.. autofunction:: ska.tmlite.server.main.update_model_layout_from_storage

.. autofunction:: ska.tmlite.server.main.update_model_layout