Models
======

.. autoclass:: ska.tmlite.models.Station
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.LayOut
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.RFI
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.SKA1Mid
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.SKA1Low
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.Instruments
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.TModelLite
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.MCCSGeoJSON
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.MCCSCrs
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.MCCSFeatures
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.MCCSGeometry
    :noindex:
    :members:

.. autoclass:: ska.tmlite.models.MCCSProperties
    :noindex:
    :members:

