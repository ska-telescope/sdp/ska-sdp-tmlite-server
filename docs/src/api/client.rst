Client
======

.. autoclass:: ska.tmlite.client.Client
   :members:

.. autoclass:: ska.tmlite.client.StandaloneClient
   :members:

.. autofunction:: ska.tmlite.client.start_local_server

