Public API
==========

The FastAPI is documented internally at ``http://localhost:80/docs`` once the server is running.
The functions and classes currently implemented in the model are:

.. toctree::

   models
   server
   client
