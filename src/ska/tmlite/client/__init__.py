import json
import multiprocessing
import socket

import httpx

from ska.tmlite.models import Instrument, LayOut

from ..models import TModelLite, Mccs
from ..server.config import Settings


class Client:
    """
    A client for the TMLite service.
    """

    def __init__(self, host="127.0.0.1", port=80):
        """
        Creates a new Client pointing to host/port.
        """
        self.url = f"http://{host}:{port}"
        self._client = httpx.AsyncClient()

    async def __aenter__(self):
        return self

    async def __aexit__(self, *_args):
        await self.aclose()

    async def aclose(self):
        """Close all underlying resources"""
        await self._client.aclose()

    def _as_json(self, model):
        # httpx's json argument expect an object that can be json-serialised,
        # but pydantic's data.dict() object isn't (e.g., it can contain a set).
        # See https://github.com/samuelcolvin/pydantic/issues/1409
        return json.loads(model.json())

    def _json_response(self, response):
        response.raise_for_status()
        return response.json()

    async def _get_json(self, path):
        return self._json_response(await self._client.get(self.url + path))

    async def _post_json(self, path, data, expect_response=False):
        response = await self._client.post(self.url + path, json=self._as_json(data))
        if expect_response:
            return self._json_response(response)
        return

    async def _put_json(self, path, data):
        return self._json_response(
            await self._client.put(self.url + path, json=self._as_json(data))
        )

    async def read_root(self) -> TModelLite:
        """Read the default Telescope Model"""
        return TModelLite.parse_obj(await self._get_json("/"))

    async def get_static_rfi_mask(self, item_id: str, instrument_id: str):
        """Get the statis RFI Mask of the requested model and instrument"""
        return await self._get_json(f"/model/{item_id}/{instrument_id}/static_rfi_mask")

    async def get_model(self, item_id: str) -> TModelLite:
        """Read the requested Telescope Model"""
        return TModelLite.parse_obj(await self._get_json(f"/model/{item_id}"))

    async def get_layout(self, item_id: str, instrument_id: str) -> LayOut:
        """Read the layout of the requested model and instrument"""
        return LayOut.parse_obj(
            await self._get_json(f"/model/{item_id}/{instrument_id}/layout")
        )

    async def get_instrument(self, item_id: str, instrument_id: str) -> Instrument:
        """Read the instrument of the requested model"""
        return Instrument.parse_obj(
            await self._get_json(f"/model/{item_id}/{instrument_id}")
        )

    async def add_model(self, item_id: str, item: TModelLite):
        """Add a new model with the givne name"""
        return await self._put_json(f"/model/{item_id}", item)

    async def update_model_antennas(
        self, item_id: str, instrument_id: str, item: Mccs
    ) -> Instrument:
        """Update the layout of the requested model and instrument"""
        return Instrument.parse_obj(
            await self._post_json(
                f"/model/{item_id}/{instrument_id}/update_antennas",
                item,
                expect_response=True,
            )
        )


def _run_local_server(host, port, settings):
    import uvicorn
    from ..server.main import app, get_settings

    app.dependency_overrides[get_settings] = lambda: settings
    uvicorn.run(app, host=host, port=port, log_level="info")


def start_local_server(host, port=None, server_settings=Settings()):
    """
    Start a TMLite server on the given host and port. It uses uvicorn to start
    the server. It returns the server process and the port to which it's bound.

    :param host: The host to bind the server to
    :param port: The port to bind the server to. If not given, 8000 is used
    :param server_settings: The settings with which the server will be started.
    """
    port = port or 8000
    server_process = multiprocessing.Process(
        target=_run_local_server, args=(host, port, server_settings)
    )
    server_process.start()
    return server_process, port


class StandaloneClient(Client):
    """
    A client that also starts its own server to talk to.
    """

    def __init__(self, port=None, server_settings=Settings()):
        """
        The host/port pair must be either given fully or not given at all. When
        they are not given, a server process is started automatically
        """
        host = "127.0.0.1"
        self.server_process, port = start_local_server(
            host, port=port, server_settings=server_settings
        )
        super().__init__(host, port)

    async def aclose(self):
        await super().aclose()
        self.server_process.kill()
        # TODO: do nicely
        self.server_process.join(10)
