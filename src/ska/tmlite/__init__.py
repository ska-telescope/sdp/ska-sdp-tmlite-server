# -*- coding: utf-8 -*-

"""Module init code."""


from . import version

__version__ = version._version

__author__ = 'Stephen Ord, Rodrigo Tobar'
__email__ = 'stephen.ord@csiro.au, rtobar@icrar.org'
