from typing import Optional, Set, List, Union

from pydantic import BaseModel

class MCCSProperties(BaseModel):
    """
    The Properties Object - contains a description of the station

    :param name: str
    :param nof_antennas: Optional[int] = None
    :param antenna_type: Optional[str] = None
    :param tpms: Optional[Set[int]] = None
    :param station_num: Optional[int] = None

    """
    name: str
    nof_antennas: Optional[int] = None
    antenna_type: Optional[str] = None
    tpms: Optional[Set[int]] = None
    station_num: Optional[int] = None

class Mccs(BaseModel):
    """
    List of antennas required in the order required
    :param station_ids: Set[int] a list of stations in the current scan
    """

    station_ids: Set[int] = set()

class TelModelFixedDelays(BaseModel):
    """
    Model for the fixed delays associated with cable lengths
    and electronic delays
    :param: fixed_delay_id: description of this delay
    :param: polarisation: which polarisaiton channel it applies to
    :param: units: the delay could be in distance or time
    :param: delay: the actual delay
    """
    fixed_delay_id: str
    polarisation: int
    units: str
    delay: float


class GeoCentricCoords(BaseModel):
    """
    Representation of a position in Geocentric Coordinates
    in units of metres

    :param: coordinate_frame: usually ITRF
    :param: Geocentric x in metres
    :param: Geocentrix y in metres
    :param: Geocentric z in metres
    """

    coordinate_frame: str
    x: float
    y: float
    z: float

class GeoDeticCoords(BaseModel):
    """
    Representation of a position in Geodetic longitude
    and latitude in radian measure and height above the
    reference ellipsoid in metres

    :param: coordinate_frame: the reference ellipsoid (WGS84?)
    :param: Geodetic latitude (radian)
    :param: Geodetic longitude (radian)
    :param: height above the ellipsoid (m)
    """
    coordinate_frame: str
    lat: float
    lon: float
    h: float

class LocalCoords(BaseModel):
    """
    Representation of a position in Local East, North, Up
    coordinates in metres
    This is referenced to a local geodetic position

    :param: coordinate_frame: local
    :param: east (m)
    :param: north (m)
    :param: up (m)
    :reference: a GEoDeticCoords object for the ref. position
    """
    coordinate_frame: str
    east: float
    north: float
    up: float
    reference: GeoDeticCoords

class TelModelLocation(BaseModel):
    """
    Representation of the location in multiple coordinate systems
    :param: Geocentric Model
    :param: Geodetic Model
    :param: Local Model
    """

    geocentric: GeoCentricCoords
    geodetic: Optional[GeoDeticCoords]
    local: Optional[LocalCoords]


class Station(BaseModel):
    """
    The schema for the SKA stations:

    :param name: (str) an identifier for the station
    :param dish_diameter: (str) dish/stations size in meters
    :param long: (str) longitude [optional]
    :param lat: (str) latitude [optional]
    :param x: (float) Geocentric x
    :param y: (float) Geocentric y
    :param z: (float) Geocentric z
    """
    interface: str
    station_name: str
    diameter: str
    properties: Optional[MCCSProperties]
    location: Optional[TelModelLocation]
    fixed_delays: List[TelModelFixedDelays] = []
    niao: float
    long: Optional[str]
    lat: Optional[str]
    x: Optional[float]
    y: Optional[float]
    z: Optional[float]

class LayOut(BaseModel):
    """
    The Schema for station layout

    :param description: str identifier
    :param reference: str where did this come from (provinence?)
    :param comment: str
    :param revision: str
    :param telescope: str telescope name (dup)
    :param coordinates: str Coordinate frame (ITRF)
    :param units: str (meters etc
    :param antennas: Set[Station] = [] an array of Station
    """

    description: Optional[str]
    reference: Optional[str]
    comment: Optional[str]
    revision: Optional[str]
    telescope: str
    coordinates: Optional[str]
    units: Optional[str]
    receptors: List[Station] = []


class RFI(BaseModel):
    """
    RFI Schema

    :param description: str identifier (what source etc)
    :param freq_start: str (Hz)
    :param freq_stop: str (Hz)
    """

    description: str
    freq_start: str
    freq_stop: str


class Instrument(BaseModel):
    """
    Base class for instruments

    :param layout: LayOut
    :param static_rfi_mask: Set[RFI] = []
    """

    layout: LayOut
    static_rfi_mask: Optional[List[RFI]] = None


class SKA1Low(Instrument):
    """
    Container item for the model for SKA1 Low

    :param layout: LayOut
    :param static_rfi_mask: Set[RFI] = []
    """


class SKA1Mid(Instrument):
    """
    Container item for the model for SKA1 Mid

    :param layout: LayOut
    :param static_rfi_mask: Set[RFI] = []
    """


class Instruments(BaseModel):
    """
    Container item for the Instrumets

    :param ska1_low: SKA1Low
    :param ska1_mid: SKA1Mid
    """

    ska1_low: Optional[SKA1Low]
    ska1_mid: Optional[SKA1Mid]


class TModelLite(BaseModel):
    """
    Container item for the whole model

    :param instrument: Instruments
    """

    instrument: Instruments


class MCCSGeometry(BaseModel):
    """
    The MCCS Geometry object - this is what actually contains the location

    :param type: str
    :param coordinates: List[float]

    """
    type: str
    coordinates: List[float]


class MCCSFeatures(BaseModel):
    """
    The Feature carries the station information in the MCCSGeoJSON
    model

    :param type: str
    :param properties: MCCSProperties
    :param geometry: MCCSGeometry

    """
    type: str
    properties: MCCSProperties
    geometry: MCCSGeometry


class MCCSCrs(BaseModel):
    """
    The CRS items in the MCCS GeoJSON file

    :param type: str
    :param properties: MCCSProperties

    """
    type: str
    properties: MCCSProperties


class MCCSGeoJSON(BaseModel):
    """
    Container item for the GeoJSON file generated by MCCS.
    Ideally we would not have to add this. But it has been deemed
    necessary to demonstrate the reading in of this file format.

    Currently,the schema is:

    :param type: str
    :param name: str
    :param crs: MCCSCrs
    :param features: List[MCCSFeatures]

    """

    type: str
    name: str
    crs: MCCSCrs
    features: List[MCCSFeatures]


def as_TModelLite(mccs_dict) -> dict:

    """
    function to take a dictionary representation of the
    MCCSGeoJSON model and present it as TModelLite

    """
    out_layout = {}
    out_layout["description"] = "Telescope State Information (TSI) - SKA1-low telescope configuration (from MCCSGeoJSON file"
    out_layout["reference"] = "unknown"
    out_layout["comment"] = "converted from the MCCSGeoJSON file"
    out_layout["revision"] = "unknown - maybe pick up from something"
    out_layout["telescope"] = "SKA1_Low"
    out_layout["coordinates"] = "ITRF"
    out_layout["units"] = "meters"

    stations = []
    for feature in mccs_dict["features"]:
        station = {}
        station["station_name"] = feature["properties"]["name"]
        station["diameter"] = "38.0"
        station["location"] = {}
        station["location"]["geodetic"] = {}
        station["location"]["geodetic"]["coordinate_frame"] = "WGS84"
        station["location"]["geodetic"]["lon"] = feature["geometry"]["coordinates"][0]
        station["location"]["geodetic"]["lat"] = feature["geometry"]["coordinates"][1]
        station["location"]["geodetic"]["h"] = 0.0
        stations.append(station)

    out_layout["receptors"] = stations
    out_model_dict = {
        "instrument" : { "ska1_low" : {"layout" : out_layout},
                         "ska1_mid" : {"layout" : out_layout}}
    }
    return out_model_dict





