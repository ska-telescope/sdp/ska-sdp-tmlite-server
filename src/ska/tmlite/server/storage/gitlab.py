import asyncio
import contextlib
import glob
import json
import logging
import os
import shutil
import tarfile
import tempfile

import gitlab


logger = logging.getLogger(__name__)

TMLITE_REPOSITORY_SLUG = "ska-telescope/sdp/ska-sdp-tmlite-repository"


def _get_clone_directory(settings):
    return os.path.abspath(
        settings.clone_directory
        or os.path.join(tempfile.gettempdir(), "tmlite_gitlab_repository")
    )


def _get_gitlab_repository(settings):
    authentication = {
        "private_token": settings.private_token,
        "job_token": settings.job_token,
        "oauth_token": settings.oauth_token,
    }
    gl = gitlab.Gitlab(**authentication)
    return gl.projects.get(TMLITE_REPOSITORY_SLUG)


def _get_default_branch(repo):
    branches = repo.branches.list()
    for branch in branches:
        if branch.attributes["default"]:
            return branch
    logger.warning("No default branch found, using first branch from the list")
    return branches[0]


def _clone(repo, ref, clone_directory):
    # Download tarball
    tarball_fd, tarball_name = tempfile.mkstemp(suffix=".tar.gz", prefix="tmlite-data-")
    with os.fdopen(tarball_fd, "wb") as wfile:
        wfile.write(repo.repository_archive(ref or _get_default_branch(repo).get_id()))

    # Extract tarballs contents and move all contents into CLONE_DIRECTORY
    try:
        with contextlib.closing(tarfile.open(tarball_name)) as tar:
            top_level_dir = tar.getnames()[0]
            tar.extractall(path=clone_directory)
            filename_patterns = ("*", ".*")
            filenames = [
                filename
                for filename_pattern in filename_patterns
                for filename in glob.glob(
                    os.path.join(clone_directory, top_level_dir, filename_pattern)
                )
            ]
            for filename in filenames:
                shutil.move(filename, clone_directory)
            os.rmdir(os.path.join(clone_directory, top_level_dir))
    finally:
        os.remove(tarball_name)


async def load_telescope_model(settings):
    """
    Load the current model from the ska-sdp-tmlite-repository GitLab repository.
    If the repository hasn't been cloned yet, it is; otherwise it is used as is,
    which allows us to use local directories as well that are not necessarily
    git repositories.
    """
    clone_directory = _get_clone_directory(settings)
    if not os.path.isdir(clone_directory):
        repo = _get_gitlab_repository(settings)
        await asyncio.get_running_loop().run_in_executor(
            None, _clone, repo, settings.repository_ref, clone_directory
        )
    # low telescope
    model = {}
    model["model"] = {}
    model["model"]["instrument"] = {}
    for instrument in ("ska1_low","ska1_mid"):
        model["model"]["instrument"][instrument] = {}
        to_load = settings.low_model
        if instrument == "ska1_mid":
            to_load = settings.mid_model

        model_to_load = []
        for entry_num,model_entry in enumerate(to_load):

            model_path = os.path.join(clone_directory, model_entry)
            with open(model_path) as model_data:
                if entry_num == 0:
                    model_to_load.append(json.load(model_data))
                else:
                    # merge the old and new dictionaries
                    model_to_load.append({**model_to_load[entry_num-1], **json.load(model_data)})

        model["model"]["instrument"][instrument]["layout"] = model_to_load[entry_num]
        model["model"]["instrument"][instrument]["static_rfi_mask"] = model_to_load[entry_num]["static_rfi_mask"]

    return model
#