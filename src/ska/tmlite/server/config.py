from typing import Optional, Union, List, Dict

from pydantic import BaseModel, BaseSettings


class GitLabStorage(BaseModel):
    clone_directory: Optional[str]
    private_token: Optional[str]
    job_token: Optional[str]
    oauth_token: Optional[str]
    model_path: str = "prototype_model.json"
    repository_ref: Optional[str]
    low_model: List = ["tmdata/instrument/ska1_low/layout/low-layout.json","tmdata/instrument/ska1_low/layout/low-rfi.json"]
    mid_model: List = ["tmdata/instrument/ska1_mid/layout/mid-layout.json","tmdata/instrument/ska1_mid/layout/mid-rfi.json"]



class Settings(BaseSettings):
    storage_backend: str = "gitlab"
    storage: Union[GitLabStorage] = GitLabStorage()

    class Config:
        env_nested_delimiter = '__'
