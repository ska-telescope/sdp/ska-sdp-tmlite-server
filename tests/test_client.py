import asyncio
import os
import unittest

from ska.tmlite.models import Mccs, TModelLite
from ska.tmlite.server.config import Settings, GitLabStorage

from .test_common import (
    assert_root,
    assert_model,
    assert_layout,
    assert_instrument,
    assert_static_rfi_mask,
    assert_updated_layout,
    model_path,
)
import json


try:
    from ska.tmlite.client import Client, StandaloneClient
except ImportError:
    Client = None


async def next_available_port():
    port = 1024
    while True:
        try:
            await asyncio.open_connection("127.0.0.1", port)
            port += 1
        except:
            break
    return port


async def wait_for_server_to_start(port):
    retries = 50
    while retries:
        try:
            await asyncio.open_connection("127.0.0.1", port)
            break
        except:
            await asyncio.sleep(0.05)
            retries -= 1
    if not retries:
        raise RuntimeError("Server did not start in 5 seconds")


async def get_standalone_client():
    port = await next_available_port()
    client = StandaloneClient(
        port=port,
        server_settings=Settings(
            storage=GitLabStorage(
                clone_directory=os.path.abspath(
                    os.path.join(os.path.dirname(__file__), "model")
                )
            ),
        ),
    )
    await wait_for_server_to_start(port)
    return client


def assert_status(status):
    def wrapper(exception):
        assert exception.response.status_code == status
    return wrapper

@unittest.skipUnless(Client, "client support not installed")
class ClientTests(unittest.TestCase):
    """Unit tests for the Client classes and functions"""

    def _test(self, *testing_steps):
        async def _run():
            client = await get_standalone_client()
            async with client:
                for client_call, assertion in testing_steps:
                    try:
                        assertion(await client_call(client))
                    except Exception as e:
                        assertion(e)

        asyncio.run(_run())

    def test_get_root(self):
        self._test((lambda client: client.read_root(), assert_root))

    def test_read_full_items(self):
        self._test((lambda client: client.get_model("current"), assert_model))

    def test_read_model_layout(self):
        self._test(
            (
                lambda client: client.get_layout("current", "ska1_low"),
                assert_layout,
            )
        )

    def test_instrument(self):
        self._test(
            (
                lambda client: client.get_instrument("current", "ska1_low"),
                assert_instrument,
            )
        )

    def test_static_rfi_mask(self):
        self._test(
            (
                lambda client: client.get_static_rfi_mask("current", "ska1_low"),
                assert_static_rfi_mask,
            )
        )

    def test_update_antennas(self):
        mccs = Mccs(station_ids=set((0, 1, 2, 3)))
        self._test(
            (
                lambda client: client.update_model_antennas(
                    "current", "ska1_low", mccs
                ),
                lambda _: None,
            ),
            (
                lambda client: client.get_layout("current", "ska1_low"),
                assert_updated_layout,
            ),
        )

    def test_update_antennas_fail(self):
        mccs = Mccs(station_ids=set((0, 1, 2, 3)))
        self._test(
            (
                lambda client: client.update_model_antennas(
                    "doesnotexist", "ska1_low", mccs
                ),
                assert_status(404)
            )
        )

    def test_update_antennas_not_allowed(self):
        mccs = Mccs(station_ids=set((0, 1, 2, 3)))
        self._test(
            (
                lambda client: client.update_model_antennas(
                    "default", "ska1_low", mccs
                ),
                assert_status(403)
            )
        )

    def test_put_model(self):
        with open(model_path) as model_file:
            new_model = json.load(model_file)

        model = {}
        model["model"] = {}
        model["model"]["instrument"] = {}
        model["model"]["instrument"]["ska1_low"] = {}
        model["model"]["instrument"]["ska1_low"]["layout"] = new_model
        model = TModelLite.parse_obj(model["model"])
        self._test(
            (
                lambda client: client.add_model("new", model),
                lambda _: None,  # assert 200
            ),
            (lambda client: client.get_model("new"), assert_model),
        )
