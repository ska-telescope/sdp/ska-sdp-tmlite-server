import os

from pydantic import BaseModel

model_dir = os.path.join(os.path.dirname(__file__), "model")
#model_path = os.path.join(model_dir, "prototype_model.json")
model_path = os.path.join(model_dir, "telmodel_schema.json")
mccs_path = os.path.join(model_dir, "station_export_w2.geojson")
update_layout_mccs = os.path.join(model_dir, "station_export_w2_update.geojson")

def decay_to_dict(f):
    """Converts first argument of `f` to a dict, assuming it's a BaseModel"""
    def wrapper(item):
        if isinstance(item, BaseModel):
            item = item.dict()
        return f(item)
    return wrapper

@decay_to_dict
def assert_root(root):
    assert (
        root["instrument"]["ska1_low"]["layout"]["receptors"][0]["diameter"]
        == "38.0"
    )

@decay_to_dict
def assert_model(model):
    assert model["instrument"]["ska1_low"]["layout"]["telescope"] == "ska1_low"

@decay_to_dict
def assert_layout(layout):
    assert layout["telescope"] == "ska1_low"

@decay_to_dict
def assert_instrument(instrument):
    assert "layout" in instrument

@decay_to_dict
def assert_static_rfi_mask(static_rfi_mask):
    assert static_rfi_mask[0]["freq_start"] == "100.0"

@decay_to_dict
def assert_updated_layout(layout):
    assert len(layout["receptors"]) == 4

@decay_to_dict
def assert_updated_layout_mccs(layout):
    assert layout["antennas"][1]["name"] == "AAVS2"
    assert len(layout["antennas"]) == 2
