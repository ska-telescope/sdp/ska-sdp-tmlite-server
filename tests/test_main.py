import json

from fastapi.testclient import TestClient

from ska.tmlite.server import config
from ska.tmlite.server.main import app, get_settings

from .test_common import (
    assert_root,
    assert_model,
    assert_layout,
    assert_instrument,
    model_dir,
)


def get_settings_override():
    return config.Settings(storage=config.GitLabStorage(clone_directory=model_dir))

app.dependency_overrides[get_settings] = get_settings_override

def test_read_root():
    with TestClient(app) as client:
        response = client.get("/")
        assert response.status_code == 200
        assert_root(response.json())

def test_read_full_items():
    with TestClient(app) as client:
        response = client.get("/model/current")
        assert response.status_code == 200
        assert_model(response.json())

def test_read_model_layout():
    with TestClient(app) as client:
        response = client.get("/model/current/ska1_low/layout")
        assert response.status_code == 200
        assert_layout(response.json())

def test_instrument():
    with TestClient(app) as client:
        response = client.get("/model/current/ska1_low")
        assert response.status_code == 200
        assert_instrument(response.json())

def test_instrument_mid():
    with TestClient(app) as client:
        response = client.get("/model/current/ska1_mid")
        assert response.status_code == 200
        assert_instrument(response.json())

