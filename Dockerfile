FROM python:3.9-slim AS builder

COPY . /app
WORKDIR /app

RUN pip install -r docker-requirements.txt
RUN pip install .

FROM python:3.9-slim
ARG IMAGE_VERSION=0.4.4

COPY --from=builder /usr/local /usr/local
LABEL \
      author="Rodrigo Tobar, Stephen Ord" \
      description="An image containing the Lite Telescope Model (TMLite)" \
      license="BSD-3-Clause" \
      vendor="SKA Telescope" \
      org.skatelescope.team="YANDA" \
      org.skatelescope.version="${IMAGE_VERSION}" \
      org.skatelescope.website="https://gitlab.com/ska-telescope/sdp/ska-sdp-tmlite-server/"

CMD ["uvicorn", "ska.tmlite.server.main:app", "--host", "0.0.0.0", "--port", "80"]
