#!/usr/bin/env python
# -*- coding: utf-8 -*-

import setuptools
from setuptools import setup

with open('README.md') as readme_file:
    readme = readme_file.read()
with open('VERSION.txt') as version_file:
    version = version_file.read().strip()
with open('src/ska/tmlite/version.py', 'w') as version_file:
    version_file.write(f'''
# This is an automatically-generated file
# DO NOT EDIT

_version = '{version}'
'''.strip())

setup(
    name='ska_sdp_tmlite_server',
    version=version,
    description="A light-weight fastAPI based server for the prototype telescope model",
    long_description=readme + '\n\n',
    author="Stephen Ord, Rodrigo Tobar",
    author_email='stephen.ord@csiro.au, rtobar@icrar.org',
    url='https://github.com/ska-telescope/ska-sdp-tmlite-server',
    packages=setuptools.find_namespace_packages(where="src", include=["ska.*"]),
    package_dir={"": "src"},
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ],
    test_suite='tests',
    install_requires=[
        'fastapi',
        'python-gitlab',
    ],
    tests_require=[
        'pytest',
        'pytest-cov',
        'pytest-json-report',
        'pycodestyle',
        'fastapi',
    ],
    extras_require={
        'client': [
            'httpx',
        ],
        'standalone-client': [
            'httpx',
            'uvicorn'
        ],
    }
)
