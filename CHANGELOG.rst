###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

[Development version]
*********************
[0.4.4]

* hotfix to deal with missing fields in the model

[0.4.3]

* Added the ska1_mid model

[0.4.2]

* changed the path for the data model to tmdata to align with telmodel expectations

[0.4.1]

* Added new model for the delay calculation
* Added example work books

[0.3.1]

* Added the ability to read MCCSGeoJSON layout files and update internal layouts using them

[0.2.2]

* Slimmed down Docker image size.
* Added simple, first version of a GitLab storage backend.
* Added simple client, with an option to start its own server automatically.
* Fix documentation build warnings.

[0.2.1]
*******

Release No functional additions - tag fixed for semantic versioning.

[0.2]
*****

Added
-----

* Added a "default" model and a "current" model. To allow for changes.
* Added a number of GET methods for aspects of the internal model
* Added a POST for update_antennas - which replaces the current antenna list
  with the new one as specified by a list of station_ids sent in the POST Body
* There is a PUT method for a new model based on JSON so you can add models to a running server.
